import "./App.css";
import Form from "./components/Form/Form";
import { Icon } from "./components/Icon/Icon";

function App() {
  const optionsArray = [
    { value: 0, title: "option => 0" },
    { value: 1, title: "option => 1" },
    { value: 2, title: "option => 2" },
    { value: 4, title: "option => 4" },
  ];
  return (
    <div>
      <Form className="form" onSubmit={(data) => console.log(data)}>
        <div>
          <Form.TextField name="name" />
          <Icon />
        </div>

        <Form.NumberField name="number" />

        <Form.Select name="select">
          {optionsArray.map((option) => (
            <Form.SelectOption value={option.value} key={option.value}>
              {option.title}
            </Form.SelectOption>
          ))}
        </Form.Select>

        <Form.Button>Submit</Form.Button>
      </Form>
    </div>
  );
}

export default App;
