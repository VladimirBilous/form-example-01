import React from "react";

export const SelectOption = (props) => {
  const { value, children } = props;

  return <option value={value}>{children}</option>;
};
