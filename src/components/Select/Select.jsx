import React, { useCallback } from "react";
import { useFormContext } from "../Form/Form";

export const Select = (props) => {
  const { id, name, children } = props;

  const { formData, handleFieldChange } = useFormContext();

  const value = formData[name] || "";

  const onChange = useCallback(
    (e) => handleFieldChange(name, e.target.value),
    [handleFieldChange, name]
  );

  return (
    <select id={id} value={value} onChange={onChange}>
      {children}
    </select>
  );
};
