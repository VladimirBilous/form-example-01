import React, { useCallback } from "react";
import { useFormContext } from "../Form/Form";

export const TextField = (props) => {
  const {
    id,
    type = "text",
    label,
    name,
    placeholder,
    minlength,
    maxlength,
    pattern,
    required = false,
  } = props;

  const { formData, handleFieldChange } = useFormContext();

  const value = formData[name] || "";

  const onChange = useCallback(
    (e) => handleFieldChange(name, e.target.value),
    [handleFieldChange, name]
  );

  return (
    <div className="textFieldContainer">
      <label>{label}</label>
      <input
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        minLength={minlength}
        maxLength={maxlength}
        pattern={pattern}
        required={required}
        {...id}
      />
    </div>
  );
};
