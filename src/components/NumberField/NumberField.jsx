import React, { useCallback } from "react";
import { useFormContext } from "../Form/Form";

export const NumberField = (props) => {
  const { id, label, name, placeholder, min, max, pattern, required } = props;

  const { formData, handleFieldChange } = useFormContext();

  const value = formData[name] || "";

  const onChange = useCallback(
    (e) => handleFieldChange(name, e.target.value),
    [handleFieldChange, name]
  );

  return (
    <div className="textFieldContainer">
      <label>{label}</label>
      <input
        type="number"
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        min={min}
        max={max}
        pattern={pattern}
        required={required}
        {...id}
      />
    </div>
  );
};
