import React, { createContext, useContext, useMemo, useState } from "react";
import { TextField } from "../TextField/TextField";
import { NumberField } from "../NumberField/NumberField";
import Button from "../Button/Button";
import { Select } from "../Select/Select";
import { SelectOption } from "../Select/SelectOption";

const FormContext = createContext(null);

export function useFormContext() {
  const context = useContext(FormContext);
  if (!context) {
    throw new Error(
      `FormContext compound components cannot be rendered outside the Form component`
    );
  }
  return context;
}

const Form = (props) => {
  const { children, initialData = {}, onSubmit } = props;

  const [formData, setFormData] = useState(initialData);

  const handleFieldChange = (name, value) => {
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(formData);
  };

  const value = useMemo(() => ({ formData, handleFieldChange }), [formData]);

  return (
    <FormContext.Provider value={value}>
      <form onSubmit={handleSubmit}>{children}</form>
    </FormContext.Provider>
  );
};

Form.TextField = TextField;
Form.NumberField = NumberField;
Form.Button = Button;
Form.Select = Select;
Form.SelectOption = SelectOption;

export default Form;
