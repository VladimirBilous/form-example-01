import { useContext } from "react";
import { FormContext } from "./formContext";

export const useFormContext = () => {
  const context = useContext(FormContext);

  if (!context) {
    throw new Error("This component must be used within a <Form> component");
  }

  return context;
};
